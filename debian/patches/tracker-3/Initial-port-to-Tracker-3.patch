From: Sam Thursfield <sam@afuera.me.uk>
Date: Fri, 15 May 2020 01:19:58 +0200
Subject: Initial port to Tracker 3

Each miner now stores its data in a private Tracker database, under
the $XDG_CACHE_HOME/gnome-online-miners/$busname directory.

Each miner now supports the org.freedesktop.Tracker3.Endpoint D-Bus
interface which apps can use to query data directly from the miners.

Origin: https://gitlab.gnome.org/GNOME/gnome-online-miners/-/merge_requests/3
---
 configure.ac          |   4 +-
 src/gom-application.c |   4 +-
 src/gom-miner.c       | 120 +++++++++++++++++++++++++++++++++++++++++++++++++-
 src/gom-miner.h       |   1 +
 src/gom-tracker.c     |   2 +-
 5 files changed, 125 insertions(+), 6 deletions(-)

diff --git a/configure.ac b/configure.ac
index e91e4af..8ee6315 100644
--- a/configure.ac
+++ b/configure.ac
@@ -21,7 +21,7 @@ AC_HEADER_STDC
 
 GDATA_MIN_VERSION=0.15.2
 GFBGRAPH_MIN_VERSION=0.2.2
-GLIB_MIN_VERSION=2.35.1
+GLIB_MIN_VERSION=2.56.0
 GOA_MIN_VERSION=3.13.3
 GRILO_MIN_VERSION=0.3.0
 ZAPOJIT_MIN_VERSION=0.0.2
@@ -36,7 +36,7 @@ PKG_CHECK_MODULES(GIO, [gio-2.0 gio-unix-2.0])
 PKG_CHECK_MODULES(GOA, [goa-1.0 >= $GOA_MIN_VERSION])
 AC_DEFINE([GOA_API_IS_SUBJECT_TO_CHANGE], [], [We are aware that GOA's API can change])
 
-PKG_CHECK_MODULES(TRACKER, [tracker-miner-2.0 tracker-sparql-2.0])
+PKG_CHECK_MODULES(TRACKER, [tracker-sparql-3.0])
 
 # Facebook
 AC_ARG_ENABLE([facebook], [AS_HELP_STRING([--enable-facebook], [Enable Facebook miner])], [], [enable_facebook=yes])
diff --git a/src/gom-application.c b/src/gom-application.c
index bfa99ec..86546ed 100644
--- a/src/gom-application.c
+++ b/src/gom-application.c
@@ -233,7 +233,9 @@ gom_application_constructed (GObject *object)
 
   G_OBJECT_CLASS (gom_application_parent_class)->constructed (object);
 
-  self->miner = g_initable_new (self->miner_type, NULL, &error, NULL);
+  self->miner = g_initable_new (self->miner_type, NULL, &error,
+                                "bus-name", g_application_get_application_id (G_APPLICATION (self)),
+                                NULL);
 
   if (self->miner == NULL)
     g_error ("%s", error->message);
diff --git a/src/gom-miner.c b/src/gom-miner.c
index 1dd9bb8..f349d01 100644
--- a/src/gom-miner.c
+++ b/src/gom-miner.c
@@ -38,7 +38,9 @@ struct _GomMinerPrivate {
   GoaClient *client;
   GError *client_error;
 
+  gchar *bus_name;
   TrackerSparqlConnection *connection;
+  TrackerEndpointDBus *endpoint;
   GError *connection_error;
 
   gchar *display_name;
@@ -62,6 +64,14 @@ typedef struct {
   gpointer service;
 } InsertSharedContentData;
 
+typedef enum
+{
+  PROP_BUS_NAME = 1,
+  N_PROPERTIES
+} GomMinerProperty;
+
+static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };
+
 static GThreadPool *cleanup_pool;
 
 static void cleanup_job (gpointer data, gpointer user_data);
@@ -135,6 +145,58 @@ gom_miner_dispose (GObject *object)
   G_OBJECT_CLASS (gom_miner_parent_class)->dispose (object);
 }
 
+static void
+gom_miner_init_database (GomMiner      *self,
+                         GCancellable  *cancellable,
+                         GError       **error)
+{
+  TrackerSparqlConnectionFlags flags;
+  g_autoptr (GFile) store_path = NULL;
+  g_autoptr (GDBusConnection) bus = NULL;
+  GError *inner_error = NULL;
+
+  flags = TRACKER_SPARQL_CONNECTION_FLAGS_FTS_ENABLE_STEMMER |
+          TRACKER_SPARQL_CONNECTION_FLAGS_FTS_ENABLE_UNACCENT |
+          TRACKER_SPARQL_CONNECTION_FLAGS_FTS_ENABLE_STOP_WORDS |
+          TRACKER_SPARQL_CONNECTION_FLAGS_FTS_IGNORE_NUMBERS;
+
+  store_path = g_file_new_build_filename (g_get_user_cache_dir (),
+                                          "gnome-online-miners",
+                                          self->priv->bus_name,
+                                          NULL);
+
+  self->priv->connection = tracker_sparql_connection_new (flags,
+                                                          store_path,
+                                                          tracker_sparql_get_ontology_nepomuk (),
+                                                          cancellable,
+                                                          &inner_error);
+
+  if (inner_error)
+    {
+      g_propagate_error (error, inner_error);
+      return;
+    }
+
+  bus = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, &inner_error);
+
+  if (inner_error)
+    {
+      g_propagate_error (error, inner_error);
+      return;
+    }
+
+  self->priv->endpoint = tracker_endpoint_dbus_new (self->priv->connection,
+                                                    bus,
+                                                    NULL, /* object path */
+                                                    cancellable,
+                                                    &inner_error);
+  if (inner_error)
+    {
+      g_propagate_error (error, inner_error);
+      return;
+    }
+}
+
 static void
 gom_miner_init_goa (GomMiner  *self,
                     GError   **error)
@@ -185,10 +247,10 @@ gom_miner_initable_init (GInitable     *initable,
 
   self = GOM_MINER (initable);
 
-  self->priv->connection = tracker_sparql_connection_get (cancellable, &inner_error);
+  gom_miner_init_database (self, cancellable, &inner_error);
   if (inner_error)
     {
-      g_propagate_prefixed_error (error, inner_error, "Unable to connect to Tracker store: ");
+      g_propagate_prefixed_error (error, inner_error, "Unable to set up Tracker database: ");
       return FALSE;
     }
 
@@ -212,6 +274,47 @@ gom_miner_init (GomMiner *self)
   self->priv->display_name = g_strdup ("");
 }
 
+static void
+gom_miner_set_property (GObject      *object,
+                        guint         property_id,
+                        const GValue *value,
+                        GParamSpec   *pspec)
+{
+  GomMiner *self = GOM_MINER (object);
+
+  switch ((GomMinerProperty) property_id)
+    {
+      case PROP_BUS_NAME:
+        g_free (self->priv->bus_name);
+        self->priv->bus_name = g_value_dup_string (value);
+        break;
+
+      default:
+        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
+        break;
+    }
+}
+
+static void
+gom_miner_get_property (GObject    *object,
+                        guint       property_id,
+                        GValue     *value,
+                        GParamSpec *pspec)
+{
+  GomMiner *self = GOM_MINER (object);
+
+  switch ((GomMinerProperty) property_id)
+    {
+       case PROP_BUS_NAME:
+         g_value_set_string (value, self->priv->bus_name);
+         break;
+
+       default:
+         G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
+         break;
+    }
+}
+
 static void
 gom_miner_initable_interface_init (GInitableIface *iface)
 {
@@ -224,6 +327,19 @@ gom_miner_class_init (GomMinerClass *klass)
   GObjectClass *oclass = G_OBJECT_CLASS (klass);
 
   oclass->dispose = gom_miner_dispose;
+  oclass->set_property = gom_miner_set_property;
+  oclass->get_property = gom_miner_get_property;
+
+  obj_properties[PROP_BUS_NAME] = g_param_spec_string ("bus-name",
+                                                       "Bus Name",
+                                                       "D-Bus name of the miner",
+                                                       NULL  /* default value */,
+                                                       G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE |
+                                                       G_PARAM_STATIC_STRINGS);
+
+  g_object_class_install_properties (oclass,
+                                     N_PROPERTIES,
+                                     obj_properties);
 
   cleanup_pool = g_thread_pool_new (cleanup_job, NULL, 1, FALSE, NULL);
 
diff --git a/src/gom-miner.h b/src/gom-miner.h
index 8f83139..5dcfc8d 100644
--- a/src/gom-miner.h
+++ b/src/gom-miner.h
@@ -61,6 +61,7 @@ typedef struct _GomMinerPrivate GomMinerPrivate;
 typedef struct {
   GomMiner *miner;
   TrackerSparqlConnection *connection;
+  gchar *bus_name;
 
   GoaAccount *account;
   GHashTable *services;
diff --git a/src/gom-tracker.c b/src/gom-tracker.c
index 68818c4..5666c16 100644
--- a/src/gom-tracker.c
+++ b/src/gom-tracker.c
@@ -408,7 +408,7 @@ gom_tracker_utils_ensure_equipment_resource (TrackerSparqlConnection *connection
   gchar *retval = NULL;
   gchar *select = NULL;
 
-  g_return_val_if_fail (TRACKER_SPARQL_IS_CONNECTION (connection), NULL);
+  g_return_val_if_fail (TRACKER_IS_SPARQL_CONNECTION (connection), NULL);
   g_return_val_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable), NULL);
   g_return_val_if_fail (error == NULL || *error == NULL, NULL);
   g_return_val_if_fail (make != NULL || model != NULL, NULL);
